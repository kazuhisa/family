class Api::V1::UsersController < ApplicationController
  doorkeeper_for :all
  before_filter :validate_token
  before_filter :set_parameters
  skip_before_filter :verify_authenticity_token # allow CSRF

  def index
    render json: current_user
  end

  def validate_token
    return head(401) unless doorkeeper_token
  end

  def set_parameters
    sign_in 'user', User.find(doorkeeper_token.resource_owner_id)
  end
end